基于WebRTC实现音视频通话

#### 使用说明

1 到服务器项目目录下webrtc-server 启动服务器端口localhost：300

```
 cd webrtc-server 
```

```
npm run start
```

2 到客户端项目目录下webrtc-server 启动服务器端口localhost：5173和5174，5173发起视频，5174接通或挂断视频

```
cd webrtc-client
```

```
npm run dev 
```

```
npm run dev 
```

![image-20241006203123000](C:\Users\Admin\AppData\Roaming\Typora\typora-user-images\image-20241006203123000.png)